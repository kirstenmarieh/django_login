# django_login
# to run this app:
    1. clone repository
    2. cd into directory
    3. type into terminal:
        i. python3 -m venv venv
        ii. source venv/bin/activate
        iii. pip install django
        iv. pip install django_plotly_dash==1.1.4
        v. pip install dash==1.4.1
        vi. pip install channels daphne redis django-redis channels-redis
        vii. python manage.py migrate
        viii. python manage.py createsuperuser (if you want to be able to login)
        viiii. python manage.py runserver
    4. to get to the starting page (dashboard, unecessary):
        i. open web browser
        ii. navigate to localhost:8000/dashboard
        iii. login to get to django dash page
    5. to view user accounts navigate to localhost:8000/admin
