from django.conf.urls import url
from users.views import dashboard, register, simple_example, view_searches
from django.urls import path , include
from users.dash_apps.finished_apps import simpleexample

urlpatterns = [
    path('accounts/', include("django.contrib.auth.urls")),
    path('dashboard/', dashboard, name = "dashboard"),
    path('register/', register, name="register"),
    path('simple_example/', simple_example, name = "simple_example"),
    path('view_searches/', view_searches, name = "view_searches"),
]