from django.shortcuts import render
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.urls import reverse
from users.forms import CustomUserCreationForm, SearchForm
from users.models import Search

# Create your views here.
def dashboard(request):
    return render(request, "users/dashboard.html", {})

def simple_example(request):
    return render(request, "users/simple_example.html", {})

def register(request):
    if request.method == "GET":
        return render(
            request, "users/register.html",
            {"form": CustomUserCreationForm}
        )
    elif request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("dashboard"))

def view_searches(request): #view user searches
    searches = Search.objects.all().order_by('-timestamp')
    form = SearchForm()
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            search = Search(
                search=form.cleaned_data["search"],
            )
            search.save()

    context = {
        "searches": searches,
        "form": form,
    }
    return render(request, "users/view_searches.html", context)