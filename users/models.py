from django.db import models

# Create your models here.
class Search(models.Model): #DB record of user search term
    search_string = models.CharField(max_length=255) #max length needed?
    timestamp = models.DateTimeField(auto_now_add=True)